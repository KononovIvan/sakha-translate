package com.yktkononov.sakhatranslate

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import com.arellomobile.mvp.MvpAppCompatActivity
import com.yktkononov.sakhatranslate.fragments.MainFragment
import com.yktkononov.sakhatranslate.fragments.PagerFragment
import com.yktkononov.sakhatranslate.fragments.TranslateFragment
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.SupportAppNavigator
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Replace
import javax.inject.Inject

const val EXTRA_MESSAGE = "com.yktkononov.sakhatranslate.message"

class MainActivityKotlin : MvpAppCompatActivity(){

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    private val navigator: Navigator = object : SupportAppNavigator(this, supportFragmentManager, R.id.fragment){

        override fun createActivityIntent(context: Context?, screenKey: String?, data: Any?): Intent? = null

        override fun createFragment(screenKey: String?, data: Any?): Fragment = when(screenKey){
                Screens.MAIN_SCREEN -> MainFragment()
                Screens.TRANSLATE_SCREEN -> TranslateFragment.newInstance(data as String)
                Screens.RANDOM_SCREEN -> PagerFragment()
                else -> throw RuntimeException("unknown screen")
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.getAppComponent().inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            navigator.applyCommands(arrayOf(BackTo(null), Replace(Screens.MAIN_SCREEN, null)))
        }
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }
}