package com.yktkononov.sakhatranslate

import com.google.gson.JsonObject
import com.yktkononov.sakhatranslate.entity.ArticleItem
import com.yktkononov.sakhatranslate.entity.Translate
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface APIService {

    @GET("suggest")
    fun getSuggest(@Query("query") query: String): Single<List<JsonObject>>

    @GET("translate")
    fun getTranslate(@Query("query") query: String): Single<Translate>

    @GET("random")
    fun getRandom(): Single<ArticleItem>
}