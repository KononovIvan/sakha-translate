package com.yktkononov.sakhatranslate.DI

import com.yktkononov.sakhatranslate.MainActivityKotlin
import com.yktkononov.sakhatranslate.fragments.MainFragment
import com.yktkononov.sakhatranslate.fragments.RandomFragment
import com.yktkononov.sakhatranslate.fragments.TranslateFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DataModule::class])
interface AppComponent{

    fun inject(mainActivityKotlin: MainActivityKotlin)
    fun inject(mainFragment: MainFragment)
    fun inject(translateFragment: TranslateFragment)
    fun inject(randomFragment: RandomFragment)
}