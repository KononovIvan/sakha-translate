package com.yktkononov.sakhatranslate.DI

import android.util.Log
import com.yktkononov.sakhatranslate.APIService
import com.yktkononov.sakhatranslate.App
import com.yktkononov.sakhatranslate.model.interactor.TranslateInteractor
import com.yktkononov.sakhatranslate.model.repository.TranslateRepository
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient) : Retrofit{
        return Retrofit.Builder()
                .baseUrl(App.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
    }

    @Provides
    @Singleton
    fun provideHttpClient() : OkHttpClient{
        val interceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message -> Log.i(App.TAG, message) })
        interceptor.level = HttpLoggingInterceptor.Level.BASIC
        return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()
    }

    @Provides
    @Singleton
    fun provideAPIService(retrofit: Retrofit) : APIService =
        retrofit.create(APIService::class.java)

    @Provides
    @Singleton
    fun provideCicerone() = Cicerone.create()

    @Provides
    @Singleton
    fun provideRouter(cicerone: Cicerone<Router>) = cicerone.router

    @Provides
    @Singleton
    fun provideNavigatorHolder(cicerone: Cicerone<Router>) = cicerone.navigatorHolder

    @Provides
    fun provideTranslateInteractor(translateRepository: TranslateRepository) = TranslateInteractor(translateRepository)

    @Provides
    fun provideTranslateRepository(apiService: APIService) = TranslateRepository(apiService)
}