package com.yktkononov.sakhatranslate

import com.hannesdorfmann.adapterdelegates3.AdapterDelegatesManager
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.yktkononov.sakhatranslate.entity.ArticleItem

class MainAdapter: ListDelegationAdapter<MutableList<Any>>() {

    init {
        items = mutableListOf()
        delegatesManager = AdapterDelegatesManager()
        delegatesManager.addDelegate(ArticleItemAdapterDelegate())
    }

    fun setData(data: List<ArticleItem>){
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }
}