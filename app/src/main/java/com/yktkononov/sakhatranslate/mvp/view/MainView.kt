package com.yktkononov.sakhatranslate.mvp.view

import com.arellomobile.mvp.MvpView

interface MainView : MvpView{

    fun showSuggest(data : List<String>)
}