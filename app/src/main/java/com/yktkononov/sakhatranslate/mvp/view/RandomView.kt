package com.yktkononov.sakhatranslate.mvp.view

import com.arellomobile.mvp.MvpView
import com.yktkononov.sakhatranslate.entity.ArticleItem

interface RandomView: MvpView {
    fun showRandom(item: ArticleItem)
    fun showProgressBar(show: Boolean)
}