package com.yktkononov.sakhatranslate.mvp.presenter

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.yktkononov.sakhatranslate.App
import com.yktkononov.sakhatranslate.Screens
import com.yktkononov.sakhatranslate.model.interactor.TranslateInteractor
import com.yktkononov.sakhatranslate.mvp.view.MainView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class MainPresenter @Inject constructor(
        private val interactor: TranslateInteractor,
        private val router: Router
) : MvpPresenter<MainView>() {

    private val disposables = CompositeDisposable()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        interactor.listenTextChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { viewState.showSuggest(it) },
                        { it.printStackTrace() }
                )
                .connect()
    }

    fun onTextChanged(text : String){
        Log.i(App.TAG, text)
        interactor.sendTextChanged(text)
    }

    fun onItemClicked(message: String) {
        router.navigateTo(Screens.TRANSLATE_SCREEN, message)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
    }

    fun onRandomClicked(){
        router.navigateTo(Screens.RANDOM_SCREEN)
    }

    private fun Disposable.connect() { disposables.add(this) }
}