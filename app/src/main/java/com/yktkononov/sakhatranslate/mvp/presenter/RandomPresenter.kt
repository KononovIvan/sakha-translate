package com.yktkononov.sakhatranslate.mvp.presenter

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.yktkononov.sakhatranslate.App
import com.yktkononov.sakhatranslate.model.interactor.TranslateInteractor
import com.yktkononov.sakhatranslate.mvp.view.RandomView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@InjectViewState
class RandomPresenter @Inject constructor(
        private val interactor: TranslateInteractor
): MvpPresenter<RandomView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        Log.i(App.TAG, "attach presenter")
    }

    private var disposable: Disposable? = null

    fun onShowRandom() {
        disposable?.dispose()
        disposable = interactor.getRandom()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgressBar(true) }
                .doAfterTerminate { viewState.showProgressBar(false) }
                .subscribe(
                        { viewState.showRandom(it) },
                        { it.printStackTrace() }
                )
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(App.TAG, "destroy presenter")
        disposable?.dispose()
    }
}