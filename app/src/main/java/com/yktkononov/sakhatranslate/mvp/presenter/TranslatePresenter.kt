package com.yktkononov.sakhatranslate.mvp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.yktkononov.sakhatranslate.model.interactor.TranslateInteractor
import com.yktkononov.sakhatranslate.mvp.view.TranslateView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@InjectViewState
class TranslatePresenter @Inject constructor(
        private val interactor: TranslateInteractor
)   : MvpPresenter<TranslateView>(){

    private var disposable: Disposable? = null

    fun onShowList(text : String) {
        disposable?.dispose()
        disposable = interactor.getTranslates(text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgressBar(true) }
                .doAfterTerminate { viewState.showProgressBar(false) }
                .subscribe(
                        { viewState.showTranslate(it) },
                        { it.printStackTrace() }
                )
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }
}