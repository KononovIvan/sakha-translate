package com.yktkononov.sakhatranslate.mvp.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.yktkononov.sakhatranslate.entity.ArticleItem
import com.yktkononov.sakhatranslate.entity.Translate

@StateStrategyType(AddToEndSingleStrategy::class)
interface TranslateView : MvpView{
    fun showTranslate(data : List<ArticleItem>)
    fun showProgressBar(show: Boolean)
}