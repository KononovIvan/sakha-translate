package com.yktkononov.sakhatranslate.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Alternatives(
        @SerializedName("@nil")
        @Expose
        val nil : Boolean
)