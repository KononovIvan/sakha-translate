package com.yktkononov.sakhatranslate.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ArticleGroup(
        @SerializedName("Articles")
        @Expose
        val articles : List<ArticleItem>,
        @SerializedName("FromLanguageName")
        @Expose
        val fromLanguage : String,
        @SerializedName("ToLanguageName")
        @Expose
        val toLanguage : String
)