package com.yktkononov.sakhatranslate.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Translate(
        @SerializedName("Alternatives")
        @Expose
        val alternatives: Alternatives,
        @SerializedName("Articles")
        @Expose
        val articles : List<ArticleGroup>,
        @SerializedName("MoreArticles")
        @Expose
        val moreArticles : List<ArticleItem>,
        @SerializedName("Query")
        @Expose
        val query : String
)