package com.yktkononov.sakhatranslate.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class ArticleItem(
        @SerializedName("CategoryName")
        @Expose
        val categoryName : String?,
        @SerializedName("FromLanguageName")
        @Expose
        val fromLanguage : String,
        @SerializedName("Id")
        @Expose
        val id : Int,
        @SerializedName("Text")
        @Expose
        val text : String,
        @SerializedName("Title")
        @Expose
        val title : String,
        @SerializedName("ToLanguageName")
        @Expose
        val toLanguage : String
): TranslateItem