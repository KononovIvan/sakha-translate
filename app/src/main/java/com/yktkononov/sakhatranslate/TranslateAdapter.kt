package com.yktkononov.sakhatranslate

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.yktkononov.sakhatranslate.entity.ArticleItem
import com.yktkononov.sakhatranslate.entity.Translate

class TranslateAdapter: BaseAdapter{

    private val items: MutableList<ArticleItem>
    private val inflater: LayoutInflater

    constructor(context: Context?, translate: Translate?){
        this.inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        this.items = mutableListOf()

        setData(translate)
    }

    fun setData(translate: Translate?){
        if(translate != null){
            for (group in  translate.articles)
                for(item in group.articles)
                    items.add(item)
            for (item in translate.moreArticles)
                items.add(item)
        }
        notifyDataSetChanged()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view: View
        val viewHolder: ViewHolder

        if(convertView == null){
            view = inflater.inflate(R.layout.list_item, parent, false)

            viewHolder = ViewHolder()
            viewHolder.titleTextView = view.findViewById(R.id.text_title)
            viewHolder.langTextView = view.findViewById(R.id.text_lang)
            viewHolder.bodyTextView = view.findViewById(R.id.text_body)

            view.tag = viewHolder
        }
        else{
            view = convertView
            viewHolder = convertView.tag as ViewHolder
        }

        val item = items[position]
        viewHolder.titleTextView.text = item.title
        viewHolder.langTextView.text = item.fromLanguage + " -> " + item.toLanguage
        viewHolder.bodyTextView.text = item.text

        return view
    }

    override fun getItem(position: Int): Any {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items.size
    }

    private class ViewHolder{
        lateinit var titleTextView: TextView
        lateinit var langTextView: TextView
        lateinit var bodyTextView: TextView
    }
}