package com.yktkononov.sakhatranslate.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.yktkononov.sakhatranslate.R
import kotlinx.android.synthetic.main.fragment_pager.*


class PagerFragment: MvpAppCompatFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_pager, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        view_pager.adapter = MyAdapter(fragmentManager)
    }

    class MyAdapter(fm: FragmentManager?): FragmentStatePagerAdapter(fm){

        override fun getItem(p0: Int) = RandomFragment.newInstance()

        override fun getCount(): Int = 100
    }
}