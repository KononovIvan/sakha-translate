package com.yktkononov.sakhatranslate.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.yktkononov.sakhatranslate.App
import com.yktkononov.sakhatranslate.R
import com.yktkononov.sakhatranslate.entity.ArticleItem
import com.yktkononov.sakhatranslate.fromHtml
import com.yktkononov.sakhatranslate.mvp.presenter.RandomPresenter
import com.yktkononov.sakhatranslate.mvp.view.RandomView
import kotlinx.android.synthetic.main.fragment_random.*
import javax.inject.Inject

class RandomFragment: MvpAppCompatFragment(), RandomView {

    @Inject
    @InjectPresenter
    lateinit var presenter: RandomPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    companion object {
        fun newInstance() = RandomFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.getAppComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_random, container, false)

        if(savedInstanceState == null)
            presenter.onShowRandom()

        return view
    }

    override fun showRandom(item: ArticleItem) {
        random_title.text = item.title
        random_lang.text = "${item.fromLanguage} -> ${item.toLanguage}"
        random_body.text = item.text.fromHtml()
    }

    override fun showProgressBar(show: Boolean) {
        random_progress.visibility = if(show) View.VISIBLE else View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(App.TAG, "onDestroy fragment")
        presenter.destroyView(this)
    }
}