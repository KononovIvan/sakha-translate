package com.yktkononov.sakhatranslate.fragments

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.yktkononov.sakhatranslate.App
import com.yktkononov.sakhatranslate.R
import com.yktkononov.sakhatranslate.mvp.presenter.MainPresenter
import com.yktkononov.sakhatranslate.mvp.view.MainView
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_main.view.*
import javax.inject.Inject

class MainFragment: MvpAppCompatFragment(), MainView{


    @Inject
    @InjectPresenter
    lateinit var presenter: MainPresenter
    @ProvidePresenter
    fun provideMainPresenter() = presenter

    private var data: MutableList<String> = mutableListOf()
    private lateinit var adapter: ArrayAdapter<String>


    override fun onCreate(savedInstanceState: Bundle?) {
        App.getAppComponent().inject(this)
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_main, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = ArrayAdapter(context, android.R.layout.simple_list_item_1, data)
        list_view.adapter = adapter
        list_view.setOnItemClickListener{_, _, position, _ -> presenter.onItemClicked(data[position])}

        edit_text.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                presenter.onTextChanged(p0.toString())
            }
        })

        fab_random.setOnClickListener{_ -> presenter.onRandomClicked() }
    }

    override fun showSuggest(data: List<String>) {
        this.data.clear()
        this.data.addAll(data)
        adapter.notifyDataSetChanged()
    }
}