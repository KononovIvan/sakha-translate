package com.yktkononov.sakhatranslate.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.yktkononov.sakhatranslate.*
import com.yktkononov.sakhatranslate.entity.ArticleItem
import com.yktkononov.sakhatranslate.mvp.presenter.TranslatePresenter
import com.yktkononov.sakhatranslate.mvp.view.TranslateView
import kotlinx.android.synthetic.main.fragment_translate.*
import kotlinx.android.synthetic.main.fragment_translate.view.*
import javax.inject.Inject

class TranslateFragment: MvpAppCompatFragment(), TranslateView{

    @Inject
    @InjectPresenter
    lateinit var presenter: TranslatePresenter

    @ProvidePresenter
    fun provideTranslatePresenter() = presenter

    private val adapter: MainAdapter by lazy { MainAdapter() }

    companion object {
        fun newInstance(data: String) = TranslateFragment().apply {
            arguments = Bundle().apply {
                putString(EXTRA_MESSAGE, data)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        App.getAppComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_translate, container, false)
        val message = "" + arguments?.getString(EXTRA_MESSAGE)

        view.translate_rv.adapter = adapter
        view.translate_rv.layoutManager = LinearLayoutManager(context)

        if (savedInstanceState == null) {
            presenter.onShowList(message)
        }

        return view
    }

    override fun showTranslate(data: List<ArticleItem>) {
        adapter.setData(data)
    }

    override fun showProgressBar(show: Boolean) {
        if(show){
            translate_rv.visibility = View.GONE
            translate_progress.visibility = View.VISIBLE
        }
        else{
            translate_rv.visibility = View.VISIBLE
            translate_progress.visibility = View.GONE
        }
    }
}