package com.yktkononov.sakhatranslate.model.repository

import com.google.gson.JsonObject
import com.yktkononov.sakhatranslate.APIService
import com.yktkononov.sakhatranslate.entity.ArticleItem
import com.yktkononov.sakhatranslate.entity.Translate
import io.reactivex.Single
import javax.inject.Inject

class TranslateRepository @Inject constructor(
        private val apiService: APIService
){
    fun getSuggests(message: String): Single<List<JsonObject>> = apiService.getSuggest(message)

    fun getTranslates(message: String): Single<Translate> = apiService.getTranslate(message)

    fun getRandom(): Single<ArticleItem> = apiService.getRandom()
}