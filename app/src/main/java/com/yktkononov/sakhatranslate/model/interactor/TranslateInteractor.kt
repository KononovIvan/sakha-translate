package com.yktkononov.sakhatranslate.model.interactor

import com.yktkononov.sakhatranslate.entity.ArticleItem
import com.yktkononov.sakhatranslate.model.repository.TranslateRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TranslateInteractor @Inject constructor(
        private val repository: TranslateRepository
){
    private val publishSubject: PublishSubject<String> = PublishSubject.create()

    fun listenTextChanged(): Observable<List<String>> = publishSubject
            .filter { it.isNotEmpty() }
            .debounce(300, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .switchMapSingle { getSuggests(it) }

    fun sendTextChanged(text: String) {
        publishSubject.onNext(text)
    }

    fun getTranslates(message: String): Single<List<ArticleItem>> =
            repository.getTranslates(message)
                    .map {
                        val list = mutableListOf<ArticleItem>()
                        it.articles.forEach { list.addAll(it.articles) }
                        list.addAll(it.moreArticles)
                        list
                    }

    private fun getSuggests(message: String): Single<List<String>> =
            repository.getSuggests(message)
                    .map { it.map { it.get("Title").asString } }

    fun getRandom(): Single<ArticleItem> = repository.getRandom()
}