package com.yktkononov.sakhatranslate

object Screens {
    const val MAIN_SCREEN = "main screen"
    const val TRANSLATE_SCREEN = "translate screen"
    const val RANDOM_SCREEN = "random screen"
}