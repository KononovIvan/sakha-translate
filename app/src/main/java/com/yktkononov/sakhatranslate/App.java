package com.yktkononov.sakhatranslate;

import android.app.Application;

import com.yktkononov.sakhatranslate.DI.AppComponent;
import com.yktkononov.sakhatranslate.DI.DaggerAppComponent;

public class App extends Application{

    public static final String TAG = "com.yktkononov.tag";
    public static final String BASE_URL = "http://sakhatyla.ru/api//articles/";

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (appComponent == null) appComponent = DaggerAppComponent.create();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
