package com.yktkononov.sakhatranslate

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.yktkononov.sakhatranslate.entity.ArticleItem
import kotlinx.android.synthetic.main.list_item.view.*

class ArticleItemAdapterDelegate: AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int): Boolean =
            items[position] is ArticleItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false))

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) =
            (holder as ViewHolder).bind(items[position] as ArticleItem)

    private inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun bind(item: ArticleItem) {
            with(itemView) {
                text_title.text = item.title
                text_lang.text = item.fromLanguage + " ->  " + item.toLanguage
                text_body.text = item.text.fromHtml()
            }
        }
    }
}