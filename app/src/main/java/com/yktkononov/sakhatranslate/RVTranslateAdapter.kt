package com.yktkononov.sakhatranslate

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.yktkononov.sakhatranslate.entity.ArticleItem
import com.yktkononov.sakhatranslate.entity.Translate

class RVTranslateAdapter(val context: Context?, val translate: Translate?): RecyclerView.Adapter<RVTranslateAdapter.ViewHolder>() {

    private val items: MutableList<ArticleItem>
    private val inflater: LayoutInflater

    init{
        this.inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        this.items = mutableListOf()

        setData(translate)
    }

    fun setData(translate: Translate?){
        items.clear()
        if(translate != null){
            for (group in  translate.articles)
                for(item in group.articles)
                    items.add(item)
            for (item in translate.moreArticles)
                items.add(item)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = inflater.inflate(R.layout.list_item, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun getItemId(position: Int): Long = position as Long

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.titleTextView.text = items[p1].title
        p0.langTextView.text = items[p1].fromLanguage + " -> " + items[p1].toLanguage
        p0.bodyTextView.text = items[p1].text
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        var titleTextView: TextView
        var langTextView: TextView
        var bodyTextView: TextView

        init {
            titleTextView = view.findViewById(R.id.text_title)
            langTextView = view.findViewById(R.id.text_lang)
            bodyTextView = view.findViewById(R.id.text_body)
        }
    }

}